package com.feinler.strawmovie.utils.token;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;


public class TokenRequestConnector {

    private static final String USERNAME_QUERY_PARAM_NAME = "username";
    private static final String PASSWORD_QUERY_PARAM_NAME = "password";

    private boolean connected;
    private String loginURI;
    private String authorizationToken;

    public TokenRequestConnector( String loginURI ) {
        this.loginURI = loginURI;
        this.connected = false;
    }

    public void connect( final String username, final String password ) throws AuthenticationFailure {
        try {
            // do request manually
            WebTarget target = ClientBuilder.newClient()
                    .target( loginURI )
                    .queryParam( USERNAME_QUERY_PARAM_NAME, username )
                    .queryParam( PASSWORD_QUERY_PARAM_NAME, password );


            Response response = target.request().post(
                    Entity.entity( new AuthenticationRequestBody(), MediaType.APPLICATION_JSON_TYPE ) );

            if ( response.getStatus() == HttpStatus.OK.value() ) {
                this.authorizationToken = response.getHeaderString( HttpHeaders.AUTHORIZATION );
                if  ( authorizationToken != null ) {
                    this.connected = true;
                }
            } else {
                //TODO remove print
                System.out.println( "Response Status: " + response.getStatus() );
            }

            throw new AuthenticationFailure();
        } catch ( Exception e ) {
            e.printStackTrace();
            throw new AuthenticationFailure();
        }
    }

    public void disconnect() {
        this.connected = false;
    }

    public Response get( String uri ) {

        return ClientBuilder
                .newClient()
                .target( uri )
                .request( MediaType.APPLICATION_JSON_TYPE )
                .header( HttpHeaders.AUTHORIZATION, authorizationToken )
                .get();
    }

    public <T> Response  post( WebTarget webTarget, Entity<T> entity ) {
        return webTarget
                .request()
                .header( HttpHeaders.AUTHORIZATION, authorizationToken )
                .post( entity );
    }

    public Response  delete( WebTarget webTarget ) {
        return webTarget
                .request()
                .header( HttpHeaders.AUTHORIZATION, authorizationToken )
                .delete();
    }


    @XmlRootElement
    public class AuthenticationRequestBody {
    }
}
